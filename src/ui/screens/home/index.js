// @flow
import { connect } from 'react-redux';
import Home from './home';


const mapStoreToProps = state => ({})

const mapDispatchToProps = dispatch => ({})

export default connect(
  mapStoreToProps,
  mapDispatchToProps)(Home);