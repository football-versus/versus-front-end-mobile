//@flow
import React from 'react'
import {
  StyleSheet,
  Text,
  View
} from 'react-native';


type Props = {};

export default class Team extends React.Component<Props> {

  render() {

    return (
      <View style={styles.container}>
        <Text {...{
          children: 'Team View Go Here?'
        }} />
      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center'
  }
});
