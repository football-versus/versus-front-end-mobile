// @flow
import { connect } from 'react-redux';
import Team from './team';


const mapStoreToProps = state => ({})

const mapDispatchToProps = dispatch => ({})

export default connect(
  mapStoreToProps,
  mapDispatchToProps)(Team);