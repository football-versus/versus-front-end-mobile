import Standings from './standings';
// @flow
import common from 'versus-common-link';
import { connect } from 'react-redux';

const mapStoreToProps = state => ({ standings: state.leaguesReducer.standings })

const mapDispatchToProps = dispatch => ({
  getLeagueStandings: league_id => { dispatch(common.actions.getLeagueStandings.submit(league_id)) }
})

export default connect(
  mapStoreToProps,
  mapDispatchToProps)(Standings);