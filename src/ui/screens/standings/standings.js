//@flow
import React from 'react';
import {
  SectionList,
  StyleSheet,
  View
} from 'react-native';
import Cell from './component/cell';
import Header from './component/header';


type Props = {};

export default class Standings extends React.Component<Props> {

  componentDidMount() {

    this.props.getLeagueStandings({ league_id: 62 })
  }

  renderSeparator = () => {

    return (
      <View
        style={{
          height: StyleSheet.hairlineWidth,
          width: '100%',
          backgroundColor: '#CED0CE',
          marginLeft: 24
        }}
      />
    );
  }

  navigate = () => {

    this.props.navigation.navigate('Team');
  }

  render() {

    return (
      <View style={styles.container}>
        <SectionList {...{
          ItemSeparatorComponent: this.renderSeparator,
          keyExtractor: (item, index) => item + index,
          renderItem: ({ item }) => (
            <Cell {...{
              ...item,
              navigate: this.navigate
            }} />
          ),
          renderSectionHeader: Header,
          sections: [{ data: this.props.standings || [] }]
        }} />
      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1
  }
});
