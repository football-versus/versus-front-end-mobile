//@flow
import React from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  View
} from 'react-native';


type Props = {};

const { width } = Dimensions.get('window');

const paddingHorizontal = width * 0.02;

const Form = () => {

  return (
    <View style={styles.container}>
      <View style={[styles.form, styles.won]}>
        <Text children={'W'} style={[styles.text]} />
      </View>
      <View style={[styles.form, styles.draw]}>
        <Text children={'D'} style={[styles.text]} />
      </View>
      <View style={[styles.form, styles.draw]}>
        <Text children={'D'} style={[styles.text]} />
      </View>
      <View style={[styles.form, styles.lost]}>
        <Text children={'L'} style={[styles.text]} />
      </View>
      <View style={[styles.form, styles.won]}>
        <Text children={'W'} style={[styles.text]} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal
  },
  draw: { backgroundColor: '#76766f' },
  form: { alignItems: 'center', borderRadius: 12, height: 24, justifyContent: 'center', marginHorizontal: 2, width: 24, },
  lost: { backgroundColor: '#d81920' },
  text: { color: '#FFF', fontWeight: '600', fontSize: 12, },
  won: { backgroundColor: '#13CF00' }
});

export default Form;
