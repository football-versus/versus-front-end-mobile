import React from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  View
} from 'react-native';


const { width } = Dimensions.get('window');

const paddingHorizontal = width * 0.02;

const Header = () => {

  return (
    <View style={[{ paddingVertical: width * 0.03 }, styles.container]}>
      <Text {...{
        children: '#',
        style: [{
          textAlign: 'center',
          width: 24 + paddingHorizontal
        }, styles.text]
      }} />
      <Text {...{
        children: 'Club',
        style: [{
          paddingLeft: 25,
          width: paddingHorizontal + 25 + width * 0.5
        }, styles.text]
      }} />
      <View style={styles.container}>
        <View style={{ flexDirection: 'row', paddingHorizontal }}>
          <Text children={'P'} style={[{ paddingHorizontal }, styles.stats, styles.text]} />
          <Text children={'W'} style={[{ paddingHorizontal }, styles.stats, styles.text]} />
          <Text children={'D'} style={[{ paddingHorizontal }, styles.stats, styles.text]} />
          <Text children={'L'} style={[{ paddingHorizontal }, styles.stats, styles.text]} />
        </View>
        <View style={{ flexDirection: 'row', paddingHorizontal }}>
          <Text children={'Gd'} style={[{ paddingHorizontal }, styles.stats, styles.text]} />
          <Text children={'Pt'} style={[{ paddingHorizontal }, styles.stats, styles.text]} />
        </View>
      </View >
      <Text children={'Form'} style={[{ flex: 1, textAlign: 'center' }, styles.text]} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: { flexDirection: 'row', backgroundColor: '#FFF' },
  stats: { width: 36 },
  text: { color: '#666', fontSize: 12, fontWeight: '600' },
});

export default Header;