//@flow
import React from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  View
} from 'react-native';


type Props = {};

const { width } = Dimensions.get('window');

const paddingHorizontal = width * 0.02;

const Statistics = ({
  overall_league_D,
  overall_league_GA,
  overall_league_GF,
  overall_league_L,
  overall_league_payed,
  overall_league_PTS,
  overall_league_W
}) => {

  const goalDifference = overall_league_GF - overall_league_GA;

  return (
    <View style={styles.container}>
      <View style={{ flexDirection: 'row', paddingHorizontal }}>
        <Text children={overall_league_payed} style={[{ paddingHorizontal }, styles.text]} />
        <Text children={overall_league_W} style={[{ paddingHorizontal }, styles.text]} />
        <Text children={overall_league_D} style={[{ paddingHorizontal }, styles.text]} />
        <Text children={overall_league_L} style={[{ paddingHorizontal }, styles.text]} />
      </View>
      <View style={{ flexDirection: 'row', paddingHorizontal }}>
        <Text children={goalDifference} style={[{ paddingHorizontal }, styles.text]} />
        <Text children={overall_league_PTS} style={{ paddingHorizontal, ...styles.text, fontWeight: '600' }} />
      </View>
    </View >
  );
}

const styles = StyleSheet.create({
  container: { flexDirection: 'row' },
  text: { fontSize: 12, width: 36 },
});

export default Statistics;
