//@flow
import React from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import Form from './form';
import Statistics from './statistics';


type Props = {};

const { width } = Dimensions.get('window');

const paddingHorizontal = width * 0.02;

const Cell = standings => {

  const position = parseInt(standings.overall_league_position, 10);
  const cellBackground = position === 1
    ? styles.first
    : position > 1 && position < 5
      ? styles.thirdToFourth
      : position === 5
        ? styles.fifth
        : styles.losers;

  return (
    <TouchableOpacity style={[styles.container, cellBackground]} onPress={standings.navigate}>
      <Text {...{
        children: position,
        style: [{
          fontWeight: '800',
          paddingHorizontal,
          width: 24
        }, styles.text]
      }} />
      <View style={{ paddingHorizontal }}>
        <Image {...{
          source: { uri: 'https://upload.wikimedia.org/wikipedia/hif/0/0d/Chelsea_FC.png' },
          style: {
            height: 25,
            width: 25
          }
        }} />
      </View>
      <View style={[{ borderRightWidth: 1, borderColor: 'tomato', borderRightWidth: 3, width: width * 0.5 }]}>
        <Text children={standings.team_name} style={styles.text} />
      </View>
      <Statistics {...{ ...standings }} />
      <Form {...{ ...standings }} />
    </TouchableOpacity >
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 44,
    paddingVertical: 12,
  },
  first: { backgroundColor: '#66ff66' },
  thirdToFourth: { backgroundColor: '#b2ffb2' },
  fifth: { backgroundColor: '#ffe4b2' },
  losers: { backgroundColor: '#FFF' },
  text: { fontSize: 12 }
});

export default Cell;
