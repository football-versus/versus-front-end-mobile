// @flow
import { connect } from 'react-redux';
import Season from './season';


const mapStoreToProps = state => ({})

const mapDispatchToProps = dispatch => ({})

export default connect(
  mapStoreToProps,
  mapDispatchToProps)(Season);