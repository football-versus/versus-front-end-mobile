// @flow
import { connect } from 'react-redux';
import Country from './country';


const mapStoreToProps = state => ({})

const mapDispatchToProps = dispatch => ({})

export default connect(
  mapStoreToProps,
  mapDispatchToProps)(Country);