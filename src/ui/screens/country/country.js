//@flow
import React from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';


type Props = {};

export default class Country extends React.Component<Props> {

  render() {

    return (
      <View style={styles.container}>
        <Text {...{
          children: 'Countries Go Here'
        }} />
      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center'
  }
});
