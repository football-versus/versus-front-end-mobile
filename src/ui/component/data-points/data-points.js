//@flow
import fp from 'lodash/fp';
import React from 'react';
import { Circle } from 'react-native-svg';


const mapWithKey = fp.map.convert({ cap: false });

const DataPoints = ({ data, onPress, x, y }) => {

  return mapWithKey((value, index) => (

    <Circle {...{
      cx: x(index),
      cy: y(value),
      fill: 'purple',
      key: index,
      onPress,
      r: 5,
      stroke: 'rgb(50, 205, 50)'
    }} />
  ), data)
}

export default DataPoints;