import React from 'react';
import {
  Alert,
  Dimensions,
  NativeModules,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View
} from 'react-native';
import { Icon, Title } from 'native-base';


const { width } = Dimensions.get('window');
const { StatusBarManager } = NativeModules;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

export default class Header extends React.Component {

  onPressButton = button => e => {

    Alert.alert(`You tapped the ${button} button`)
  }

  render() {

    return (
      <View style={styles.container}>
        <TouchableOpacity {...{
          onPress: this.onPressButton('Drawer'),
          style: {
            alignItems: 'center',
            flex: 1,
            flexDirection: 'row',
            height: '100%',
            paddingLeft: 10
          }
        }}>
          <Icon {...{
            name: 'menu',
            size: 20,
            style: { color: 'tomato' },
            type: 'MaterialIcons'
          }} />
        </TouchableOpacity>
        <View {...{
          style: {
            alignItems: 'center',
            flex: 1,
            flexDirection: 'row',
            height: '100%',
            justifyContent: 'center'
          }
        }}>
          <Title {...{
            children: 'Versus'
          }} />
        </View>
        <View {...{
          style: {
            alignItems: 'center',
            flex: 1,
            flexDirection: 'row',
            height: '100%',
            justifyContent: 'flex-end',
          }
        }}>
          <TouchableOpacity {...{
            onPress: this.onPressButton('Profile'),
            style: { paddingRight: 5 }
          }}>
            <Icon {...{
              name: 'person',
              size: 20,
              type: 'MaterialIcons'
            }} />
          </TouchableOpacity>
          <TouchableOpacity {...{
            onPress: this.onPressButton('Search'),
            style: { paddingRight: 10 }
          }}>
            <Icon {...{
              name: 'search',
              size: 20,
              type: 'EvilIcons'
            }} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'rgb(250, 250, 250)',
    borderBottomColor: 'rgb(233, 233, 233)',
    borderBottomWidth: StyleSheet.hairlineWidth,
    flexDirection: 'row',
    height: APPBAR_HEIGHT + STATUSBAR_HEIGHT,
    justifyContent: 'space-around',
    paddingTop: STATUSBAR_HEIGHT,
    width
  }
})