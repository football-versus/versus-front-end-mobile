import { Icon } from 'native-base';
import React from 'react';
import { TouchableOpacity } from 'react-native';


const RightButton = (

  <TouchableOpacity style={{ paddingRight: 5 }}>
    <Icon {...{
      name: 'person',
      size: 20,
      type: 'MaterialIcons'
    }} />
  </TouchableOpacity>
)

export default RightButton;