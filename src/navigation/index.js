import {
  createAppContainer,
  createStackNavigator
} from 'react-navigation'
import Stack from './stack'
import LeftButton from './left-button';
import RightButton from './right-button';


const DrawerNavigation = createStackNavigator({
  DrawerStack: {
    screen: Stack,
    headerMode: 'float',
    initialRoute: Stack,
    navigationOptions: ({ navigation: { toggleDrawer } }) => ({
      headerLeft: LeftButton({ toggleDrawer }),
      headerRight: RightButton,
      headerStyle: { backgroundColor: 'rgb(250, 250, 250)' },
      headerTintColor: 'black',
      title: 'Versus'
    })
  }
})

export default createAppContainer(DrawerNavigation);

