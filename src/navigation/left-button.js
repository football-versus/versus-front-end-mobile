import { Icon } from 'native-base';
import React from 'react';
import { TouchableOpacity } from 'react-native';


const LeftButton = ({ toggleDrawer }) => (

  <TouchableOpacity {...{
    onPress: toggleDrawer,
    style: {
      alignItems: 'center',
      flex: 1,
      flexDirection: 'row',
      height: '100%',
      paddingLeft: 5
    }
  }}>
    <Icon {...{
      name: 'menu',
      size: 20,
      style: { color: 'tomato' },
      type: 'MaterialIcons'
    }} />
  </TouchableOpacity>
)

export default LeftButton;