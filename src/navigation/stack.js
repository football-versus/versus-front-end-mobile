import { createDrawerNavigator, createStackNavigator } from 'react-navigation'
import Country from '../ui/screens/country';
import Home from '../ui/screens/home';
import Season from '../ui/screens/season';
import Standings from '../ui/screens/standings';
import Team from '../ui/screens/team';


const TeamStack = createStackNavigator({
  Standings: { screen: Standings, navigationOptions: { header: null }  },
  Team: { screen: Team },
}, {
  initialRouteName: 'Standings',
})

const Stack = createDrawerNavigator({
  Standings: { screen: TeamStack },
  Home: { screen: Home },
  Country: { screen: Country },
  Season: { screen: Season },
})

export default Stack;