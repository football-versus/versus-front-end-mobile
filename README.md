# [Versus - Front End Mobile App Repository]
---
[![Total alerts](https://img.shields.io/lgtm/alerts/b/football-versus/versus-front-end-mobile.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/b/football-versus/versus-front-end-mobile/alerts/)
[![Language grade: JavaScript](https://img.shields.io/lgtm/grade/javascript/b/football-versus/versus-front-end-mobile.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/b/football-versus/versus-front-end-mobile/context:javascript)

## Installing Dependencies

You will need Node, Watchman, the React Native command line interface, and Xcode.

While you can use any editor of your choice to develop your app, you will need to install Xcode in order to set up the necessary tooling to build your React Native app for iOS.

## Node, Watchman

Run the following commands in a Terminal after installing Homebrew

`brew install node`
`brew install watchman`

## The React Native CLI

Run the following command in a Terminal to install the react native CLI globally

`npm install -g react-native-cli`

## Installation

Change directory into the mobile project's root `cd mobile`

Run `npm run start` to start react packager server

## Fire up simulation

In a new terminal window/tab run `react-native run-ios --simulatpr="iPhone 8"`. 

For a list of possible versions open xcode and click the drop down button on the top left corner.

Similarly to run on an android emulator run `react-native run-android`

To successfully fire an emulator android studio must have configured as well as csettin gup ompatible emulator that plays well with react native.